<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CountryCatalogController;
use App\Http\Controllers\SearchCountryController;

Route::controller(CountryCatalogController::class)->group(function() {
   Route::get('flags', 'showCountryAll');
});

Route::controller(SearchCountryController::class)->group(function() {
   Route::get('search/{name}', 'searchCountryName');
});