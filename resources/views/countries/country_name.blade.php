<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Document</title>
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</head>
<body>
   <div class="container-fluid">

      @php
         $data = collect($data);
      @endphp

      {{-- Country Name (name.official) --}}
      <h1 class="text-center mt-5"> - Country Name (name.official)</h1>
      <div class="row gy-3">
         @foreach ($data as $item)
            <div class="col-sm-6 col-md-6 col-lg-3">
               <ul>
                  <li>{{$item['name']['common']}}</li>
               </ul>
            </div>
         @endforeach
      </div>
   </div>
</body>
</html>