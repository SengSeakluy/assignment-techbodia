<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Document</title>
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</head>
<body>
   <div class="container-fluid">
      @php
         $data = collect($data);
      @endphp

      {{-- Flags --}}
      <h1 class="text-center"> - Flags (Please use png file within flags property)</h1>
      <div class="row gy-3">
         @foreach ($data as $item)
            <div class="col-sm-6 col-md-6 col-lg-3">
               <img src="{{$item['flags']['png']}}" class="img-fluid">
            </div>
         @endforeach
      </div>

      {{-- Country Name (name.official) --}}
      <h1 class="text-center mt-5"> - Country Name (name.official)</h1>
      <div class="row gy-3">
         @foreach ($data as $item)
            <div class="col-sm-6 col-md-6 col-lg-3">
               <ul>
                  <li>{{$item['name']['official']}}</li>
               </ul>
            </div>
         @endforeach
      </div>

      {{-- 2 character Country Code (cca2) --}}
      <h1 class="text-center mt-5"> - 2 character Country Code (cca2)</h1>
      <div class="row gy-3">
         @foreach ($data as $item)
            <div class="col-sm-6 col-md-6 col-lg-3">
               <ul>
                  <li>{{$item['cca2']}}</li>
               </ul>
            </div>
         @endforeach
      </div>

      {{-- 3 character Country Code (cca3) --}}
      <h1 class="text-center mt-5"> - 3 character Country Code (cca3)</h1>
      <div class="row gy-3">
         @foreach ($data as $item)
            <div class="col-sm-6 col-md-6 col-lg-3">
               <ul>
                  <li>{{$item['cca3']}}</li>
               </ul>
            </div>
         @endforeach
      </div>

      {{-- Native Country Name (name.nativeName) --}}{{-- @dump($item) --}}
      <h1 class="text-center mt-5"> - Native Country Name (name.nativeName)</h1>
      <div class="row gy-3">
         @foreach ($data as $item)
            <div class="col-sm-6 col-md-6 col-lg-3">
               @isset($item['name']['nativeName'])
                  @foreach($item['name']['nativeName'] as $lang => $nativeNames)
                     @foreach ($nativeNames as $key => $value)
                        <div class="card">
                           <div class="card-body">
                              {{$lang}} | {{$key}} | {{$value}}
                           </div>
                        </div>
                     @endforeach
                  @endforeach
               @endisset
            </div>
         @endforeach
      </div>


      {{-- Alternative Country Name (altSpellings) --}}
      <h1 class="text-center mt-5"> - Alternative Country Name (altSpellings)</h1>
      <div class="row gy-3">
         @foreach ($data as $item)
            <div class="col-sm-6 col-md-6 col-lg-3">
               @foreach ($item['altSpellings'] as $value)
                  <ul>
                     <li>{{$value}}</li>
                  </ul>
               @endforeach
            </div>
         @endforeach
      </div>


      {{--  Country Calling Codes (idd) --}}
      <h1 class="text-center mt-5"> - Country Calling Codes (idd)</h1>
      <div class="row gy-3">
         @foreach ($data as $item)
            @isset($item['idd']['root'])
            <div class="col-sm-6 col-md-6 col-lg-3">
               <ul>
                  <li>{{$item['idd']['root']}}</li>
               </ul>
            </div>
            @endisset
         @endforeach
      </div>

      {{-- - Search by Country Name (Fuzzy Search) --}}
      {{-- <h1 class="text-center mt-5"> - Search by Country Name (Fuzzy Search)</h1>
      <div class="row gy-3">
         @foreach ($data as $item)
            @isset($item['idd']['root'])
            <div class="col-sm-6 col-md-6 col-lg-3">
               <ul>
                  <li>{{$item['idd']['root']}}</li>
               </ul>
            </div>
            @endisset
         @endforeach
      </div> --}}

      {{-- Sorting by Country Name (Asc,Desc) --}}
      <h1 class="text-center mt-5"> - Sorting by Country Name (Asc)</h1>
      <div class="row gy-3">
         @foreach ($data->sortBy('name') as $item)
            <div class="col-sm-6 col-md-6 col-lg-3">
               <ul>
                  <li>{{$item['name']['common']}}</li>
               </ul>
            </div>
         @endforeach
      </div>

      <h1 class="text-center mt-5"> - Sorting by Country Name (Desc)</h1>
      <div class="row gy-3">
         @foreach ($data->sortByDesc('name') as $item)
            <div class="col-sm-6 col-md-6 col-lg-3">
               <ul>
                  <li>{{$item['name']['common']}}</li>
               </ul>
            </div>
         @endforeach
      </div>

      {{-- Pagination (25 rows per page) --}}
      <h1 class="text-center mt-5"> - Pagination (25 rows per page)</h1>
      <div class="row gy-3">
         @foreach ($data->take('25') as $key => $item)
            <div class="card">
               <div class="card-body">
                  <ul>
                     <li>{{$key+ 1}} -- {{$item['name']['common']}}</li>
                  </ul>
               </div>
            </div>
         @endforeach
      </div>

      {{-- After clicked on country name, pop up a modal and show all others informations. --}}
      <h1 class="text-center mt-5">After clicked on country name, pop up a modal and show all others informations.</h1>
      <!-- Button trigger modal -->
      @foreach ($data as $key => $item)
         <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#exampleModal{{$key}}">
            {{$item['name']['common']}}
         </button>
      
         <!-- Modal -->
         <div class="modal fade" id="exampleModal{{$key}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
         
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"> {{$item['name']['common']}}</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
               </div>
               <div class="modal-body">
                  <ul>
                     <li>{{$item['name']['official']}}</li>
                  </ul>

                  @isset($item['name']['nativeName'])
                     @foreach($item['name']['nativeName'] as $lang => $nativeNames)
                        @foreach ($nativeNames as $key => $value)
                           <div class="card">
                              <div class="card-body">
                                 {{$lang}} | {{$key}} | {{$value}}
                              </div>
                           </div>
                        @endforeach
                     @endforeach
                  @endisset
               </div>
         
            </div>
            </div>
         </div>
      @endforeach

   </div>
</body>
</html>