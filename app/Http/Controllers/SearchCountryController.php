<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SearchCountryController extends Controller
{
    //
    public function searchCountryName($name)
    {
        try {
            $response = $this->getApiData($name);
            if ($response->getStatusCode() === 200) {
                $data = json_decode($response->getBody(), true);
                
                return view('countries.country_name', compact('data'));
            }else{
                return view('error', ['message' => 'Not Fount.']);
            }
        }catch(Exception $e) {
            $e->getMessage();
        }
    }

    private function getApiData($name)
    {
        $client = new Client();
        $url = 'https://restcountries.com/v3.1/name/'.$name;
        $response =  $client->request('GET', $url);
        return $response;
    }
}