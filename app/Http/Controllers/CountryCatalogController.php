<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CountryCatalogController extends Controller
{
    //
    public function showCountryAll()
    {
        try {
            $response = $this->getApiData();
            if ($response->getStatusCode() === 200) {
                $data = json_decode($response->getBody(), true);
                
                return view('countries.index', compact('data'));
            }else{
                return view('error', ['message' => 'Not Fount.']);
            }
        }catch(Exception $e) {
            $e->getMessage();
        }
    }

    private function getApiData()
    {
        $client = new Client();
        $url = 'https://restcountries.com/v3.1/all';
        $response =  $client->request('GET', $url);
        
        return $response;
    }
    
}